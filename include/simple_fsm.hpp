#pragma once

#include <cstdint>
#include <optional>
#include <tuple>
#include <type_traits>
#include <variant>

namespace simple_fsm {

    struct initial {};
    struct terminating {};
    struct no_action {};

    using invalid_state = std::monostate;

    template<typename T, T... arguments>
    struct simple_matcher {
        using type = T;

        constexpr bool accepts(T argument) const { return ((argument == arguments) || ...); }
    };

    template<typename FuncType>
    struct func_matcher {
        using func_type = FuncType;

        constexpr func_matcher(func_type cond) : m_cond(cond) {}

        template<typename T>
        constexpr bool accepts(T argument) const {
            return m_cond(argument);
        }

       private:
        func_type m_cond;
    };

    template<typename FuncType>
    func_matcher(FuncType func)->func_matcher<FuncType>;

    namespace detail {

        template<typename FsmType, typename InputType, ssize_t Index,
                 std::enable_if_t<Index == -1, decltype(Index)> = 0>
        constexpr auto process_event_helper(FsmType, typename FsmType::fsm_execution_type, InputType) {
            return typename FsmType::fsm_execution_type{};
        }

        template<typename FsmType, typename InputType, ssize_t Index,
                 std::enable_if_t<Index != -1, decltype(Index)> = 0>
        constexpr auto process_event_helper(FsmType state_machine,
                                            typename FsmType::fsm_execution_type execution_context, InputType input) ->
            typename FsmType::fsm_execution_type {
            static_assert(std::tuple_size_v<typename FsmType::transition_type> != 0,
                          "Invalid state machíne, no transitions are defined");

            auto current_state = execution_context.m_current_state;
            auto current_transition = std::get<Index>(state_machine.m_transitions);

            using from_state_type = typename decltype(current_transition)::from_state_type;
            if (std::holds_alternative<from_state_type>(current_state) &&
                std::get<from_state_type>(current_state) == current_transition.m_from &&
                current_transition.m_transition_condition.accepts(input)) {
                return execution_context.template next_state<decltype(current_transition.m_to), InputType>(
                    current_transition.m_to, input);
            }

            return process_event_helper<FsmType, InputType, Index - 1>(state_machine, execution_context, input);
        }

        struct empty_type_list {};

        template<typename Front, typename... Tail>
        struct type_list final {
            using front = Front;
            using tail = type_list<Tail...>;

            template<typename Type>
            static inline constexpr bool contains_v = std::is_same_v<Type, Front> ||
                                                      (std::is_same_v<Type, Tail> || ...);

            using make_unique =
                std::conditional_t<tail::make_unique::template contains_v<front>, typename tail::make_unique,
                                   typename tail::make_unique::template prepend<front>>;

            template<typename... Types>
            using append = type_list<Front, Tail..., Types...>;

            template<typename... Types>
            using prepend = type_list<Types..., Front, Tail...>;

            // template<typename ListType>
            // using append_list = typename ListType::template append<Front, Tail...>;
            //
            // template<typename ListType>
            // using prepend_list = typename ListType::template prepend<Front, Tail...>;

            using variant_type = std::variant<Front, Tail...>;
            using tuple_type = std::tuple<Front, Tail...>;

            static inline constexpr size_t len = sizeof...(Tail) + 1;
        };

        template<typename Front>
        struct type_list<Front> final {
            using front = Front;
            using tail = type_list<empty_type_list>;

            template<typename Type>
            static inline constexpr bool contains_v = std::is_same_v<Type, Front>;

            using make_unique = type_list<Front>;

            template<typename Type>
            using append = type_list<Front, Type>;

            template<typename Type>
            using prepend = type_list<Type, Front>;

            using variant_type = std::variant<Front>;
            using tuple_type = std::tuple<Front>;

            static inline constexpr size_t len = 1;
        };

        template<>
        struct type_list<empty_type_list> final {
            using front = empty_type_list;
            using tail = type_list<empty_type_list>;

            using make_unique = type_list<empty_type_list>;

            template<typename>
            static inline constexpr bool contains_v = false;

            template<typename Type>
            using append = type_list<Type>;

            template<typename Type>
            using prepend = append<Type>;

            static inline constexpr size_t len = 0;
        };

        template<typename First, typename... Args>
        constexpr uint16_t occurences(First first, Args... args) {
            return ((first == args) + ... + 0);
        }

        template<typename... Args>
        constexpr bool is_set(Args... args) {
            return ((occurences(args, args...) == 1) && ... && true);
        }

    }  // namespace detail

    template<typename FsmType, typename InputType>
    constexpr auto process_event(FsmType state_machine, typename FsmType::fsm_execution_type execution_context,
                                 InputType input) {
        return detail::process_event_helper<FsmType, InputType,
                                            std::tuple_size_v<typename FsmType::transition_type> - 1>(
            state_machine, execution_context, input);
    }

    namespace detail {
#ifndef MAX_CONSTEXPR_LEN
#define MAX_CONSTEXPR_LEN 10000
#endif
        inline constexpr size_t max_constexpr_len = MAX_CONSTEXPR_LEN;
    }  // namespace detail

    namespace detail {
        template<typename FsmType, typename InputType, size_t Len, std::enable_if_t<(Len < max_constexpr_len), int> = 0>
        constexpr auto process_events(FsmType state_machine, typename FsmType::fsm_execution_type execution_context,
                                      const InputType (&input)[Len], size_t index) ->
            typename FsmType::fsm_execution_type {
            if (index >= Len) {
                return execution_context;
            }

            return process_events(state_machine, process_event(state_machine, execution_context, input[index]), input,
                                  index + 1);
        }

        template<typename FsmType, typename InputType, size_t Len,
                 std::enable_if_t<(Len >= max_constexpr_len), int> = 0>
        auto process_events(FsmType state_machine, typename FsmType::fsm_execution_type execution_context,
                            const InputType (&input)[Len], size_t) -> typename FsmType::fsm_execution_type {
            for (size_t i = 0; i < Len; ++i) {
                execution_context = process_event(state_machine, execution_context, input[i]);
            }

            return execution_context;
        }
    }  // namespace detail

    template<typename FsmType, typename InputType, size_t Len>
    constexpr auto process_events(FsmType state_machine, typename FsmType::fsm_execution_type execution_context,
                                  const InputType (&input)[Len]) {
        return detail::process_events(state_machine, execution_context, input, 0);
    }

    template<typename InitialStateType, typename... StateTypes>
    class state_set {
       public:
        using type_list_type = typename detail::type_list<InitialStateType, StateTypes...>;
        using initial_state_type = InitialStateType;

        constexpr state_set(InitialStateType initial_state, StateTypes... states)
            : m_states(initial_state, states...) {}

        constexpr initial_state_type initial_state() const { return std::get<0>(m_states); }

       private:
        typename type_list_type::tuple_type m_states;
    };

    template<typename StateFuncType = no_action, typename StateEntryFuncType = no_action,
             typename StateExitFuncType = no_action>
    class state_action {
       public:
        using state_function_type = StateFuncType;
        using state_entry_function_type = StateEntryFuncType;
        using state_exit_function_type = StateExitFuncType;

        constexpr state_action(state_function_type func = state_function_type{})
            : state_action(func, no_action{}, no_action{}) {}
        constexpr state_action(const state_action &other) = default;
        constexpr state_action(state_action &&other) = default;

        constexpr state_action &operator=(const state_action &other) = default;
        constexpr state_action &operator=(state_action &&other) = default;

        template<typename StateEntryFuncTypeNew>
        constexpr auto on_entry(StateEntryFuncTypeNew new_entry_func) const {
            return state_action<state_function_type, StateEntryFuncTypeNew, state_exit_function_type>(
                m_state_func, new_entry_func, m_exit_func);
        }

        template<typename StateExitFuncTypeNew>
        constexpr auto on_exit(StateExitFuncTypeNew new_exit_func) const {
            return state_action<state_function_type, state_entry_function_type, StateExitFuncTypeNew>(
                m_state_func, m_entry_func, new_exit_func);
        }

        constexpr auto state_function() const { return m_state_func; }

        constexpr auto entry_function() const { return m_entry_func; }

        constexpr auto exit_function() const { return m_exit_func; }

       private:
        constexpr state_action(state_function_type func, state_entry_function_type entry_func,
                               state_exit_function_type exit_func)
            : m_state_func(func), m_entry_func(entry_func), m_exit_func(exit_func) {}

        state_function_type m_state_func;
        state_entry_function_type m_entry_func;
        state_exit_function_type m_exit_func;

        template<typename A, typename B, typename C>
        friend class state_action;
    };

    namespace detail {

        template<typename ActionType>
        struct retrieve_action_type {
            using type = state_action<ActionType>;
        };

        template<typename ActionType, typename EntryType, typename ExitType>
        struct retrieve_action_type<state_action<ActionType, EntryType, ExitType>> {
            using type = state_action<ActionType, EntryType, ExitType>;
        };

    }  // namespace detail

    template<typename IdType, typename ActionType = no_action, typename FirstProperty = detail::empty_type_list,
             typename... Args>
    class state {
       public:
        using id_type = IdType;
        using action_type = ActionType;
        using state_action_type = typename detail::retrieve_action_type<ActionType>::type;
        using type_list_type = typename detail::type_list<FirstProperty, Args...>::make_unique;

        static inline constexpr bool is_terminating = type_list_type::template contains_v<terminating>;
        static inline constexpr bool is_initial_state = type_list_type::template contains_v<initial>;

        constexpr state(id_type id) : m_id(id) {}
        constexpr state(id_type id, action_type action) : m_id(id), m_action(action) {}

        template<typename ExecutionContext, typename InputType>
        constexpr auto state_action(ExecutionContext context, InputType input) const {
            if constexpr (!std::is_same_v<typename state_action_type::state_function_type, no_action>) {
                return m_action.state_function()(context, input);
            } else {
                // To prevent the unused variables warning
                (void)context;
                (void)input;
                return;
            }
        }

        template<typename ExecutionContext, typename InputType>
        constexpr auto entry_action(ExecutionContext context, InputType input) const {
            if constexpr (!std::is_same_v<typename state_action_type::state_entry_function_type, no_action>) {
                return m_action.entry_function()(context, input);
            } else {
                // To prevent the unused variables warning
                (void)context;
                (void)input;
                return;
            }
        }

        template<typename ExecutionContext, typename InputType>
        constexpr auto exit_action(ExecutionContext context, InputType input) const {
            if constexpr (!std::is_same_v<typename state_action_type::state_exit_function_type, no_action>) {
                return m_action.exit_function()(context, input);
            } else {
                // To prevent the unused variables warning
                (void)context;
                (void)input;
                return;
            }
        }

        constexpr const auto &action() const { return m_action; }

        constexpr const auto &id() const { return m_id; }

       private:
        id_type m_id{};
        state_action_type m_action{};
    };

    template<typename StateLhs, typename StateRhs,
             typename = std::void_t<typename StateLhs::action_type, typename StateRhs::action_type>>
    constexpr bool operator==(const StateLhs &lhs, const StateRhs &rhs) {
        return lhs.id() == rhs.id();
    }

    template<typename StateLhs, typename StateRhs,
             typename = std::void_t<typename StateLhs::action_type, typename StateRhs::action_type>>
    constexpr bool operator!=(const StateLhs &lhs, const StateRhs &rhs) {
        return !(lhs == rhs);
    }

    template<typename StateSetType>
    class fsm_execution final {
       public:
        using state_set_type = StateSetType;
        using state_variant_type =
            typename state_set_type::type_list_type::make_unique::template append<std::monostate>::variant_type;

        constexpr fsm_execution() = default;
        template<typename CurrentStateType>
        constexpr fsm_execution(CurrentStateType current_state) : m_current_state(current_state) {}

        template<typename T, typename InputType>
        constexpr fsm_execution next_state(T next, InputType current_input) const {
            auto is_different_state = !std::holds_alternative<std::decay_t<T>>(m_current_state) ||
                                      std::get<std::decay_t<T>>(m_current_state) != next;
            auto next_execution = fsm_execution(next);
            if (is_different_state) {
                std::visit(
                    [&next_execution, &current_input](const auto &current_state) {
                        // TODO: think about wether to pass the old execution context here or the new one
                        if constexpr (!std::is_same_v<std::decay_t<decltype(current_state)>, std::monostate>) {
                            current_state.exit_action(next_execution, current_input);
                        }
                    },
                    m_current_state);
                next.entry_action(next_execution, current_input);
            }

            next.state_action(next_execution, current_input);
            return next_execution;
        }

        state_variant_type m_current_state{std::monostate{}};
    };

    template<typename StateSetType, typename TransitionType>
    class fsm final {
       public:
        using state_set_type = StateSetType;
        using transition_type = TransitionType;
        using fsm_execution_type = fsm_execution<state_set_type>;

        static inline constexpr bool is_valid = true;

        constexpr fsm(state_set_type states, TransitionType transitions)
            : m_states(states), m_transitions(transitions) {}

        template<std::enable_if_t<is_valid, int> = 0>
        constexpr fsm_execution_type create_execution_context() const {
            return fsm_execution_type(m_states.initial_state());
        }

        const state_set_type m_states;
        const transition_type m_transitions;
    };

    template<typename TransitionConditionType, typename StateLhsType, typename StateRhsType>
    struct transition final {
        using from_state_type = StateLhsType;
        using to_state_type = StateRhsType;
        using transition_condition_type = TransitionConditionType;

        constexpr transition(transition_condition_type transition_condition, from_state_type from, to_state_type to)
            : m_transition_condition(transition_condition), m_from(from), m_to(to) {}

        const transition_condition_type m_transition_condition;
        const from_state_type m_from;
        const to_state_type m_to;
    };

    template<typename TransitionConditionType, typename StateLhsType, typename StateRhsType>
    transition(TransitionConditionType transition_condition, StateLhsType lhs, StateRhsType rhs)
        ->transition<TransitionConditionType, StateLhsType, StateRhsType>;

    template<typename InitialStateType, typename... StateTypes>
    constexpr std::optional<state_set<InitialStateType, StateTypes...>> make_state_set(InitialStateType initial_state,
                                                                                       StateTypes... states) {
        if (!detail::is_set(initial_state, states...)) {
            return std::nullopt;
        }

        return state_set<InitialStateType, StateTypes...>{initial_state, states...};
    }

    template<typename StateSetType, typename TransitionType,
             typename = std::void_t<typename StateSetType::initial_state_type>>
    constexpr auto operator|(StateSetType lhs, TransitionType rhs) {
        return fsm<StateSetType, std::tuple<TransitionType>>{lhs, std::make_tuple(rhs)};
    }

    template<typename StateSetType, typename TransitionsType, typename TransitionType,
             typename = std::void_t<typename fsm<StateSetType, TransitionsType>::fsm_execution_type>>
    constexpr auto operator|(const fsm<StateSetType, TransitionsType> lhs, const TransitionType rhs) {
        using fsm_type = fsm<StateSetType, TransitionsType>;
        auto new_transitions = std::tuple_cat(lhs.m_transitions, std::make_tuple(rhs));

        return fsm<typename fsm_type::state_set_type, decltype(new_transitions)>{lhs.m_states, new_transitions};
    }

    template<typename StateSetType, typename TransitionType,
             typename = std::void_t<typename StateSetType::variant_type>>
    constexpr auto operator|(TransitionType lhs, StateSetType rhs) {
        return rhs | lhs;
    }

    template<typename StateSetType, typename TransitionsType, typename TransitionType>
    constexpr auto operator|(const TransitionType lhs, const fsm<StateSetType, TransitionsType> rhs) {
        return rhs | lhs;
    }
}  // namespace simple_fsm
