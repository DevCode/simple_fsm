from conans import ConanFile, CMake, tools

class SimpleFSMConan(ConanFile):
    name = "SimpleFSM"
    version = "0.1"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Simplejsonpp here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "build_tests" : [True, False], "create_compile_commands" : [True, False]}
    exports_sources = "CMakeLists.txt", "conanfile.py", "include/*", "examples/*", "tests/*"
    default_options = "shared=False", "build_tests=True", "create_compile_commands=True"
    generators = "cmake"
    requires = ()

    def configure(self):
        if self.options.build_tests:
            self.requires("catch2/2.5.0@bincrafters/stable")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_EXPORT_COMPILE_COMMANDS"] = self.options.create_compile_commands
        cmake.definitions["ENABLE_TESTING"] = self.options.build_tests
        cmake.configure()
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        cmake.test(target="basic_tests_exe")

        if self.settings.os == "Linux":
            self.run("make test")

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_id(self):
        self.info.header_only()

