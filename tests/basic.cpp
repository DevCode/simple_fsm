#include <cxxabi.h>
#include <experimental/random>
#include <functional>
#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include "simple_fsm.hpp"

enum struct return_type { invalid, valid, unused };

enum struct simple_id { one, two, three, four };

class state_handler {
   public:
    template<typename T, typename InputType>
    constexpr auto operator()(T, InputType input) const {
        return input;
    }
};

TEST_CASE("typelist tests") {
    using namespace simple_fsm;

    using start_type_list = detail::type_list<int, double, float>;

    static_assert(std::is_same_v<detail::type_list<detail::empty_type_list>::append<void>, detail::type_list<void>>,
                  "Append from empty list should be list with the one type");
    static_assert(start_type_list::template contains_v<int> && start_type_list::template contains_v<double> &&
                      start_type_list::template contains_v<float>,
                  "start_type_list has to contain these types");
    static_assert(detail::type_list<void>::contains_v<void>, "Th contain bool should also work with a singular type");
    static_assert(!detail::type_list<void>::contains_v<char>, "Th contain bool should also work with a singular type");
    static_assert(!start_type_list::template contains_v<char>, "start_type_list shouldn't contain this type");
    static_assert(std::is_same_v<start_type_list::append<char>, detail::type_list<int, double, float, char>>,
                  "start_type_list with char appended has to be the type_list<int, double, float, char>");
    static_assert(std::is_same_v<start_type_list::variant_type, std::variant<int, double, float>>);
    static_assert(std::is_same_v<start_type_list::tuple_type, std::tuple<int, double, float>>);
    static_assert(start_type_list::len == 3, "The number of types in the start_type_list should be 3");
    static_assert(detail::type_list<detail::empty_type_list>::len == 0, "The size of an empty list should be zero");

    static_assert(std::is_same_v<detail::type_list<void, int, void>::make_unique, detail::type_list<int, void>>,
                  "Has to be the same type");
    static_assert(
        std::is_same_v<detail::type_list<void, int, void, double>::make_unique, detail::type_list<int, void, double>>,
        "Has to be the same type");
    static_assert(std::is_same_v<detail::type_list<void, int, void, double, double, void>::make_unique,
                                 detail::type_list<int, double, void>>,
                  "Has to be the same type");
    static_assert(std::is_same_v<detail::type_list<void, int, void, double, double, void, float>::make_unique,
                                 detail::type_list<int, double, void, float>>,
                  "Has to be the same type");
    static_assert(
        std::is_same_v<detail::type_list<int, double, float>::make_unique, detail::type_list<int, double, float>>,
        "Has to be the same type");

    REQUIRE(true);
}

TEST_CASE("Basic state_action tests") {
    using namespace simple_fsm;

    constexpr auto lambda = [](auto &current_char) { return current_char == 'a'; };
    constexpr auto entry_lambda = [](auto &current_char) { return current_char == 'b'; };
    constexpr auto exit_lambda = [](auto &current_char) { return current_char == 'c'; };

    state_action action{lambda};
    static_assert(std::is_same_v<decltype(action)::state_function_type, std::decay_t<decltype(lambda)>>,
                  "Check if the types of the first state_action are correct");
    static_assert(std::is_same_v<decltype(action)::state_entry_function_type, no_action>,
                  "Check if the types of the first state_action are correct");
    static_assert(std::is_same_v<decltype(action)::state_exit_function_type, no_action>,
                  "Check if the types of the first state_action are correct");

    auto second_state_action = action.on_exit(exit_lambda);

    static_assert(std::is_same_v<decltype(second_state_action)::state_function_type, std::decay_t<decltype(lambda)>>,
                  "Check if the types of the first state_action are correct");
    static_assert(
        !std::is_same_v<decltype(second_state_action)::state_exit_function_type, std::decay_t<decltype(lambda)>>,
        "Check if the types of the first state_action are correct");
    static_assert(
        std::is_same_v<decltype(second_state_action)::state_exit_function_type, std::decay_t<decltype(exit_lambda)>>,
        "Check if the types of the first state_action are correct");
    static_assert(std::is_same_v<decltype(second_state_action)::state_entry_function_type, no_action>,
                  "Check if the types of the first state_action are correct");

    auto third_state_action = second_state_action.on_entry(entry_lambda);

    static_assert(std::is_same_v<decltype(third_state_action)::state_function_type, std::decay_t<decltype(lambda)>>,
                  "Check if the types of the first state_action are correct");
    static_assert(
        std::is_same_v<decltype(third_state_action)::state_exit_function_type, std::decay_t<decltype(exit_lambda)>>,
        "Check if the types of the first state_action are correct");
    static_assert(
        !std::is_same_v<decltype(third_state_action)::state_entry_function_type, std::decay_t<decltype(lambda)>>,
        "Check if the types of the first state_action are correct");
    static_assert(
        std::is_same_v<decltype(third_state_action)::state_entry_function_type, std::decay_t<decltype(entry_lambda)>>,
        "Check if the types of the first state_action are correct");

    state_action empty_action{};
    static_assert(std::is_same_v<decltype(empty_action)::state_function_type, no_action>,
                  "Check if a there is a valid no_action state_action");

    // TODO: add some more test cases to test if the lambdas are copied correctly
}

TEST_CASE("Basic state tests") {
    using namespace simple_fsm;

    constexpr auto handler = [](const auto &, const auto &input) -> bool { return input; };
    constexpr auto entry_handler = [](const auto &, const auto &input) -> int { return input + 4; };
    constexpr auto exit_handler = [](const auto &, const auto &input) -> int { return input + 20; };

    constexpr state simple_state(simple_id::one, handler);
    constexpr state second_simple_state(simple_id::two, state_handler{});
    constexpr state third_simple_state{simple_id::three};
    constexpr state fourth_simple_state_with_special_handler{
        simple_id::four, state_action(handler).on_entry(entry_handler).on_exit(exit_handler)};

    static_assert(!decltype(simple_state)::is_terminating, "This state shouldn't be terminating");
    static_assert(!decltype(simple_state)::is_initial_state, "This state shouldn't be terminating");
    static_assert(std::is_same_v<std::decay_t<decltype(std::declval<decltype(simple_state)>().action())>,
                                 state_action<std::decay_t<decltype(handler)>>>,
                  "The stored handler in the state should be the same as the one that was provided to the constructor");
    static_assert(!simple_state.action().state_function()(0, 0), "Check if the constexpr lambda works as expected");
    static_assert(simple_state.action().state_function()(0, 1), "Check if the constexpr lambda works as expected");
    static_assert(simple_state.state_action(0, 1), "Check if the constexpr lambda works as expected");
    static_assert(!simple_state.state_action(0, 0), "Check if the constexpr lambda works as expected");
    static_assert(!second_simple_state.action().state_function()(0, 0),
                  "Check if the constexpr operator() of state_handler works as expected");
    static_assert(second_simple_state.action().state_function()(0, 1),
                  "Check if the constexpr operator() of state_handler works as expected");
    static_assert(!second_simple_state.state_action(0, 0),
                  "Check if the constexpr operator() of state_handler works as expected");
    static_assert(second_simple_state.state_action(0, 1),
                  "Check if the constexpr operator() of state_handler works as expected");
    static_assert(simple_state.id() == simple_id::one, "Check if the id is set correctly");
    static_assert(second_simple_state.id() == simple_id::two, "Check if the id is set correctly");
    static_assert(third_simple_state.id() == simple_id::three, "Check if the id is set correctly");

    static_assert(fourth_simple_state_with_special_handler.action().state_function()(0, 0) == 0,
                  "Check if the state function is correct");
    static_assert(fourth_simple_state_with_special_handler.action().entry_function()(0, 0) == 4,
                  "Check if the entry function is correct");
    static_assert(fourth_simple_state_with_special_handler.action().exit_function()(0, 0) == 20,
                  "Check if the exit function is correct");

    // If it compiles the tests were successfull
    REQUIRE(true);
}

TEST_CASE("Basic matcher tests") {
    using namespace simple_fsm;

    constexpr auto lambda_func = [](auto current_char) -> auto { return current_char == 'c' || current_char == 'd'; };

    simple_matcher<char, 'a', 'b'> matcher{};
    func_matcher second_matcher(lambda_func);

    static_assert(matcher.accepts('a'), "Check if the matcher accepts the correct input");
    static_assert(matcher.accepts('b'), "Check if the matcher accepts the correct input");
    static_assert(!matcher.accepts('c'), "Check if the matcher accepts the correct input");

    static_assert(second_matcher.accepts('c'), "Check if the matcher accepts the correct input");
    static_assert(second_matcher.accepts('d'), "Check if the matcher accepts the correct input");
    static_assert(!second_matcher.accepts('e'), "Check if the matcher accepts the correct input");

    // If it compiles successfully, the tests were successfull
    REQUIRE(true);
}

TEST_CASE("Basic state_set tests") {
    using namespace simple_fsm;

    constexpr auto handler = [](const auto &, const auto &input) -> bool { return input; };

    using first_state_type = state<simple_id, decltype(handler)>;
    using second_state_type = state<simple_id, state_handler>;
    using third_state_type = state<simple_id, no_action>;

    constexpr first_state_type simple_state(simple_id::one, handler);
    constexpr second_state_type second_simple_state(simple_id::two, state_handler{});
    constexpr third_state_type third_simple_state{simple_id::three};

    constexpr auto set = *make_state_set(simple_state, second_simple_state, third_simple_state);

    static_assert(set.initial_state() == simple_state, "Initial state has to be set correctly");
    static_assert(std::is_same_v<decltype(set)::type_list_type,
                                 detail::type_list<first_state_type, second_state_type, third_state_type>>,
                  "Check if the type_list type is correct");

    constexpr auto invalid_set = make_state_set(simple_state, simple_state);
    static_assert(!invalid_set.has_value(), "Check if invalid set really produces an invalid state_set");

    constexpr auto second_invalid_set = make_state_set(simple_state, second_simple_state, simple_state);
    static_assert(!second_invalid_set.has_value(), "Invalid set shouldn't have a value");

    constexpr auto third_invalid_set = make_state_set(simple_state, second_simple_state, second_simple_state);
    static_assert(!third_invalid_set.has_value(), "Invalid set shouldn't have a value");

    constexpr auto valid_set = make_state_set(simple_state, second_simple_state, third_simple_state);
    static_assert(valid_set.has_value(), "A valid set should return a value");

    // If it compiles the tests were successfull
    REQUIRE(true);
}

TEST_CASE("Basic transition tests") {
    using namespace simple_fsm;

    constexpr state<simple_id, no_action, initial> first_state(simple_id::one);
    constexpr state<simple_id, no_action, terminating> second_state(simple_id::two);
    constexpr simple_matcher<return_type, return_type::invalid> m;
    constexpr simple_matcher<return_type, return_type::valid> m2;
    constexpr simple_matcher<return_type, return_type::valid, return_type::invalid> m3;

    auto first_transition = transition(m, first_state, second_state);
    auto second_transition = transition(m2, second_state, first_state);
    auto third_transition = transition(m3, second_state, first_state);

    static_assert(!std::is_same_v<decltype(first_transition), decltype(second_transition)>,
                  "These should be different types");
    static_assert(
        std::is_same_v<std::decay_t<decltype(first_transition)::from_state_type>, std::decay_t<decltype(first_state)>>,
        "These should be the same types");
    static_assert(std::is_same_v<std::decay_t<decltype(second_transition)::from_state_type>,
                                 std::decay_t<decltype(second_state)>>,
                  "These should be the same types");

    static_assert(first_transition.m_transition_condition.accepts(return_type::invalid), "Should accept this value");
    static_assert(!first_transition.m_transition_condition.accepts(return_type::valid), "Shouldn't accept this one");
    static_assert(second_transition.m_transition_condition.accepts(return_type::valid), "Should accept this value");
    static_assert(!second_transition.m_transition_condition.accepts(return_type::invalid),
                  "Shouldn't accept this one ");
    static_assert(third_transition.m_transition_condition.accepts(return_type::valid) &&
                      third_transition.m_transition_condition.accepts(return_type::invalid),
                  "Should accept both values");
}

TEST_CASE("Basic state set test") {
    using namespace simple_fsm;

    constexpr state<simple_id, no_action, initial, terminating> initial_state(simple_id::one);
    constexpr state<simple_id, no_action, initial, terminating> second_state(simple_id::two);
    constexpr state<simple_id, no_action, initial, terminating> second_state_eq(simple_id::two);
    constexpr state<simple_id, no_action, initial, terminating> third_state(simple_id::three);

    static_assert(!make_state_set(initial_state, second_state, second_state_eq).has_value(),
                  "State set is supposed to be invalid");
    auto second_state_set = make_state_set(initial_state, second_state, third_state);
    auto second_state_set_val = second_state_set.value();
    // static_assert(second_state_set.has_value(), "State set is supposed to be valid");
    static_assert(std::is_same_v<decltype(second_state_set_val)::type_list_type,
                                 detail::type_list<state<simple_id, no_action, initial, terminating>,
                                                   state<simple_id, no_action, initial, terminating>,
                                                   state<simple_id, no_action, initial, terminating>>>,
                  "State set is supposed to be valid");

    static_assert(std::is_same_v<fsm_execution<decltype(second_state_set_val)>::state_variant_type,
                                 std::variant<std::decay_t<decltype(initial_state)>, std::monostate>>,
                  "Check if the variant type is correct");
}

TEST_CASE("Basic usage tests") {
    using namespace simple_fsm;

    constexpr state_handler handler;
    constexpr state<simple_id, state_handler, initial, terminating> initial_state(simple_id::one, handler);
    constexpr state<simple_id, state_handler, terminating> other_state(simple_id::two, handler);
    constexpr simple_matcher<return_type, return_type::invalid> invalid_matcher;
    constexpr simple_matcher<return_type, return_type::valid> valid_matcher;

    // TODO: maybe add transition callbacks
    constexpr auto states = make_state_set(initial_state, other_state);
    static_assert(states.has_value(), "This has to be a valid set");
    constexpr auto machine = states.value() | transition(valid_matcher, initial_state, other_state) |
                             transition(invalid_matcher, initial_state, initial_state) |
                             transition(valid_matcher, other_state, other_state) |
                             transition(invalid_matcher, other_state, initial_state);
    constexpr auto execution_context = machine.create_execution_context();

    constexpr return_type input[3]{return_type::valid, return_type::valid, return_type::invalid};
    constexpr auto result = process_events<decltype(machine), return_type, 3>(machine, execution_context, input);
    constexpr auto second_result = process_event(machine, result, return_type::valid);
    constexpr auto third_invalid_result = process_event(machine, result, return_type::unused);

    static_assert(std::holds_alternative<std::decay_t<decltype(initial_state)>>(result.m_current_state),
                  "Correct state after the inputs");
    static_assert(std::holds_alternative<std::decay_t<decltype(other_state)>>(second_result.m_current_state),
                  "Correct state after the single input");
    static_assert(std::holds_alternative<invalid_state>(third_invalid_result.m_current_state),
                  "Invalid state after the invalid input");

    REQUIRE(std::holds_alternative<invalid_state>(third_invalid_result.m_current_state));
    REQUIRE(std::holds_alternative<std::decay_t<decltype(initial_state)>>(result.m_current_state));
    REQUIRE(std::holds_alternative<std::decay_t<decltype(other_state)>>(second_result.m_current_state));
}

TEST_CASE("EntryExit Function Test") {
    using namespace simple_fsm;

    int check_number = 0;
    constexpr int start_value = 0;
    int index = start_value;
    int results[8]{};
    constexpr int inc_state = 1;
    constexpr int inc_entry = 2;
    constexpr int inc_exit = 3;
    constexpr simple_matcher<return_type, return_type::invalid> invalid_matcher;
    constexpr simple_matcher<return_type, return_type::valid> valid_matcher;
    auto initial_state_actions = state_action([&check_number, &index, &results](auto, auto) {
                                     check_number += inc_state;
                                     results[index++] = check_number;
                                     return;
                                 })
                                     .on_entry([&check_number, &index, &results](auto, auto) {
                                         check_number += inc_entry;
                                         results[index++] = check_number;
                                         return;
                                     })
                                     .on_exit([&check_number, &index, &results](auto, auto) {
                                         check_number += inc_exit;
                                         results[index++] = check_number;
                                         return;
                                     });

    state<simple_id, decltype(initial_state_actions), terminating, initial> initial_state(simple_id::one,
                                                                                          initial_state_actions);
    state<simple_id, decltype(initial_state_actions)> second_state(simple_id::two, initial_state_actions);

    auto set = make_state_set(initial_state, second_state);

    REQUIRE(set.has_value());

    if (!set) {
        REQUIRE(false);
        return;
    }

    auto set_val = *set;

    auto state_machine = set_val | transition(invalid_matcher, initial_state, second_state) |
                         transition(valid_matcher, initial_state, initial_state) |
                         transition(invalid_matcher, second_state, second_state) |
                         transition(valid_matcher, second_state, initial_state);

    constexpr return_type input[4] = {return_type::invalid, return_type::invalid, return_type::valid,
                                      return_type::valid};
    process_events(state_machine, state_machine.create_execution_context(), input);

    // TODO the semantics aren't 100% clear right now, just check if every action is executed at some point
    // The order right now is switch, stay, switch, stay
    // leave
    REQUIRE(results[0] == start_value + inc_exit);
    // entry
    REQUIRE(results[1] == start_value + inc_exit + inc_entry);
    // stay
    REQUIRE(results[2] == start_value + inc_exit + inc_entry + inc_state);

    // stay
    REQUIRE(results[3] == start_value + inc_exit + inc_entry + inc_state * 2);

    // leave
    REQUIRE(results[4] == start_value + inc_exit * 2 + inc_entry + inc_state * 2);
    // entry
    REQUIRE(results[5] == start_value + inc_exit * 2 + inc_entry * 2 + inc_state * 2);
    // stay
    REQUIRE(results[6] == start_value + inc_exit * 2 + inc_entry * 2 + inc_state * 3);

    // stay
    REQUIRE(results[7] == start_value + inc_exit * 2 + inc_entry * 2 + inc_state * 4);
}

TEST_CASE("Parsing constexpr string test") {
    static constexpr char input[4] = "aab";

    using namespace simple_fsm;

    constexpr state<simple_id, no_action, initial> start(simple_id::one);
    constexpr state<simple_id, no_action> middle(simple_id::two);
    constexpr state<simple_id, no_action, terminating> end(simple_id::three);

    constexpr simple_matcher<char, 'a'> a_matcher;
    constexpr simple_matcher<char, 'b', '\0'> b_matcher;

    constexpr auto machine = make_state_set(start, middle, end).value() | transition(a_matcher, start, middle) |
                             transition(a_matcher, middle, middle) | transition(b_matcher, middle, end) |
                             transition(b_matcher, end, end);
    constexpr auto execution_context = machine.create_execution_context();

    constexpr auto result = process_events(machine, execution_context, input);

    static_assert(std::holds_alternative<std::decay_t<decltype(end)>>(result.m_current_state));
}

TEST_CASE("Runtime test") {
    using namespace simple_fsm;

    constexpr state_handler handler;
    constexpr state<simple_id, state_handler, initial, terminating> initial_state(simple_id::one, handler);
    constexpr state<simple_id, state_handler, terminating> other_state(simple_id::two, handler);
    constexpr simple_matcher<return_type, return_type::invalid> invalid_matcher;
    constexpr simple_matcher<return_type, return_type::valid> valid_matcher;

    constexpr auto machine =
        make_state_set(initial_state, other_state).value() | transition(valid_matcher, initial_state, other_state) |
        transition(invalid_matcher, initial_state, initial_state) |
        transition(valid_matcher, other_state, other_state) | transition(invalid_matcher, other_state, initial_state);
    constexpr auto execution_context = machine.create_execution_context();

    constexpr size_t size = 1000;
    return_type input[size]{};

    for (size_t i = 0; i < size; ++i) {
        input[i] = std::experimental::randint(0, 1) == 0 ? return_type::invalid : return_type::valid;
    }

    auto result = process_events(machine, execution_context, input);
    auto second_result = process_event(machine, result, return_type::valid);

    if (input[size - 1] == return_type::invalid) {
        REQUIRE(std::holds_alternative<std::decay_t<decltype(initial_state)>>(result.m_current_state));
    } else if (input[size - 1] == return_type::valid) {
        REQUIRE(std::holds_alternative<std::decay_t<decltype(other_state)>>(result.m_current_state));
    }

    REQUIRE(std::holds_alternative<std::decay_t<decltype(other_state)>>(second_result.m_current_state));

    // The fsm doesn't have a valid transition, so this input should produce an invalid result
    auto third_invalid_result = process_event(machine, second_result, return_type::unused);

    REQUIRE(std::holds_alternative<invalid_state>(third_invalid_result.m_current_state));
}
