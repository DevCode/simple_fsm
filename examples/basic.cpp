#include <cxxabi.h>
#include <array>
#include <experimental/random>
#include <iostream>

#include "simple_fsm.hpp"

enum struct return_type : uint8_t { invalid, valid };

class state_handler {
   public:
    template<typename ExecutionContext, typename InputType>
    constexpr void operator()(ExecutionContext , InputType ) const {
        // std::cout << context.m_current_state.index() << std::endl;
    }
};

enum struct simple_id { one, two };

constexpr bool operator==(const state_handler &lhs, const state_handler &rhs) { return true; }

int main() {
    using namespace simple_fsm;

    constexpr state_handler handler;
    constexpr state<simple_id, state_handler, initial, terminating> initial_state(simple_id::one, handler);
    constexpr state<simple_id, state_handler, terminating> other_state(simple_id::two, handler);
    constexpr simple_matcher<return_type, return_type::invalid> invalid_matcher;
    constexpr simple_matcher<return_type, return_type::valid> valid_matcher;

    // TODO: maybe add transition callbacks
    constexpr auto machine = make_state_set(initial_state, other_state).value() |
                             transition(valid_matcher, initial_state, other_state) |
                             transition(invalid_matcher, initial_state, initial_state) |
                             transition(valid_matcher, other_state, other_state) |
                             transition(invalid_matcher, other_state, initial_state);
    constexpr auto execution_context = machine.create_execution_context();

    constexpr size_t size = 5'000'000;
    return_type input[size]{};

    for (size_t i = 0; i < size; ++i) {
        input[i] = std::experimental::randint(0, 1) == 0 ? return_type::invalid : return_type::valid;
    }

    size_t index = 0;
    auto result = process_events(machine, execution_context, input);
    auto second_result = process_event(machine, result, return_type::valid);
    index += second_result.m_current_state.index();

    return index;
}
